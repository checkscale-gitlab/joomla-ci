[![Build Status](https://gitlab.com/slavanap/joomla-ci/badges/master/build.svg)](https://gitlab.com/slavanap/joomla-ci/pipelines)

## Joomla automated builds

In order to test it install Docker, run
`docker run -itd --name test -p 80:80 registry.gitlab.com/slavanap/joomla-ci`
and open http://localhost/
